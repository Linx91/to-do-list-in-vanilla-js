let tbody,
  storage,
  myTasks = {};
const classMap = {
  0: "table-danger",
  1: "table-warning",
  2: "table-success"
};

function init() {
  storage = localStorage;
  tbody = document.querySelector("#taskList tbody");
  myTasks = localStorage.getItem("myTasks")
    ? JSON.parse(storage.getItem("myTasks"))
    : {};

  if (Object.keys(myTasks).length > 0) {
    Object.values(myTasks).forEach(task => {
      paintTask(task);
    });
  }
}

init();

function Task(name, startDate, dueDate) {
  this.name = name;
  this.startDate = startDate;
  this.dueDate = dueDate;
  this.status = "0";
  this.creationDate = Date.now();
  this.id = Date.now() + getRandomString(3);
}

function getRandomString(length) {
  return Math.random()
    .toString(36)
    .slice(-length);
}

document.getElementById("addTaskBtn").addEventListener("click", e => {
  e.preventDefault();
});

function addTask() {
  console.log("adding task");
  const form = document.forms[0];
  console.log(form, form.taskName.value);
  const newTask = new Task(
    form.taskName.value,
    form.startDate.value,
    form.dueDate.value
  );

  myTasks[newTask.id] = newTask;
  window.localStorage.setItem("myTasks", JSON.stringify(myTasks));
  console.log("new task", newTask, JSON.stringify(myTasks));
  console.log("taks list", window.localStorage.getItem("myTasks"));

  paintTask(newTask);
  console.log("body", tbody);
}

function paintTask(task) {
  tbody.innerHTML += `  
  <tr class=${classMap[task.status]} data-id=${task.id}>
  <td>${task.name}</td>
  <td>${task.startDate}</td>
  <td>${task.dueDate}</td>
    <td>
        <select
        name="taskStatus"
        id="taskStatus"
        onchange="statusChanged(this.value, this.parentNode.parentNode)"
        class="form-control"
        >
        <option value="0" ${
          task.status === "0" ? "selected" : ""
        }>Yet to Start</option>
        <option value="1" ${
          task.status === "1" ? "selected" : ""
        }>In progress</option>
        <option value="2" ${
          task.status === "2" ? "selected" : ""
        }>Complete</option>
        </select>
    </td>
    <td>
    <button class="btn btn-secondary" onclick="deleteTask(this.parentNode.parentNode)"><i class="fas fa-trash-alt"></i></button>
    </td>
    </tr>
  `;
}

function deleteTask(parentTR) {
  console.log("delete", parentTR, parentTR.dataset.id);
  tbody.removeChild(parentTR);
  delete myTasks[parentTR.dataset.id];
  updateStorge();
}

function updateStorge() {
  window.localStorage.setItem("myTasks", JSON.stringify(myTasks));
}

function statusChanged(value, parentTR) {
  parentTR.setAttribute("class", classMap[value]);
  myTasks[parentTR.dataset.id].status = value;
  updateStorge();
  console.log("status", parentTR.dataset);
}
